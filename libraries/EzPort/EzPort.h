/*
 *
 *
 */
 
#pragma once

/* EZPort commands */
#define WREN 0x06
#define WRDI 0x04
#define RDSR 0x05
#define READ 0x03
#define SP   0x02
#define BE   0xC7
#define SE	 0xD8

#define shit

/* EZPort status register */
#define WIP     0x01
#define WEN     0x02
#define BEDIS   0x04
#define FLEXRAM 0x08
#define WEF     0x40
#define FS      0x80


class EzPort {
public:
	EzPort(uint8_t slaveSelectPin, uint8_t resetPin)
	{
		_slaveSelectPin=slaveSelectPin;
		_resetPin=resetPin;
		pinMode(_resetPin, INPUT_PULLUP); // set the pins loosely pulled high.
		pinMode(_slaveSelectPin, INPUT_PULLUP); // allows target to self propel them with vague encouragement to be 'HIGH'
		SPI.begin();
	}
	void open();
	void close();
	uint8_t readStatus();
	bool writeEnable(bool en=true);
	void read(uint32_t address, uint8_t* ptr);
	
	bool bulkErase();
	bool sectorErase(uint32_t address);
	
	void sectorSize(uint16_t n) { _sector_size=n; }
	void writeCaveat(uint16_t n) { _writing_caveat=n;}
//	void currentPtr(uint32_t n) { _current_ptr=n; }
//	uint32_t getPtr(void) { return _current_ptr; }
	
	
	uint16_t write(uint32_t address, uint8_t* buffer, uint16_t length);
	
	
	
	

protected:
    bool select(uint8_t fast=1);
	void unselect();
	
	uint8_t _resetPin=0;
	uint8_t _slaveSelectPin=0;
	uint16_t _sector_size=2048;
	uint16_t _writing_caveat=0; // 416 bytes for kinetis etc
//	uint32_t _current_ptr=0;
	
	uint8_t cst=0;
};