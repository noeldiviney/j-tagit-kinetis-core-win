#define adau1372_I2C	0x3C

#include "AudioControl.h"

class AudioControlADAU1372 : public AudioControl
{

public:
	bool enableSlave(uint8_t power_down_pin);
	unsigned char writeReg(uint8_t regNum, uint8_t value);
	unsigned char readReg(uint8_t regNum);
	bool enable(void) { return false; }
	bool disable(void) { return false; }
	bool volume(float volume) { return false; }      // volume 0.0 to 1.0
	bool inputLevel(float volume) { return false; } // volume 0.0 to 1.0
	bool inputSelect(int n) { return false; }
	
	
	void inputControl(bool in0, bool in1, bool in2, bool in3);
	void inputControl(uint8_t inNum, bool state);
	void inputLevel(uint8_t inNum, float setting);
	void inputLevel(uint8_t inNum, uint8_t setting);
	void outputControl(bool left, bool right);
	void outputControl(uint8_t outNum, bool state);

	void outputLevel(uint8_t outNum, float setting);
	void outputLevel(uint8_t outNum, uint8_t setting);
	// void pgaControl(uint8_t chanNum, bool state, bool slew, bool boost, uint8_t slewRate=2);
	void pgaSlewRate(uint8_t slewRate);
	void pgaEnable(uint8_t chanNum, bool state);
	void pgaBoost(uint8_t chanNum, bool boost);
	void pgaSlew(uint8_t chanNum, bool slew);
	void pgaLevel(uint8_t chanNum, uint8_t level);
	void micBias(uint8_t chanNum, uint8_t level);
	
	void passThru(bool en);
};

