


#ifndef _fasterBiquad_h_
#define _fasterBiquad_h_

#include "Arduino.h"
#include "AudioStream48.h"


#ifndef COEFFICIENTS_FLOAT_T
struct coefficients_float_t {
	float b0;
	float b1;
	float b2;
	float a1;
	float a2;
	float aprev1;
	float aprev2;
	float bprev1;
	float bprev2;
	float sum;
};
#define FILTER_COEFS_PROMISE 32767
#define FILTER_COEFS_LOAD_CONTD(b0,b1,b2,a1,a2) {b0,b1,b2,a1,a2,0,0,0,0,FILTER_COEFS_PROMISE}
#define FILTER_COEFS_LOAD_FINISH(b0,b1,b2,a1,a2) {b0,b1,b2,a1,a2,0,0,0,0,0}

#define FILTER_COEFS_PASS_CONTD {1,0,0,0,0,0,0,0,0,32767}
#define FILTER_COEFS_PASS_FINISH {1,0,0,0,0,0,0,0,0,0}

#define COEFFICIENTS_FLOAT_T FILTER_COEFS_FINISH
#endif
#ifndef FILTER_TYPES_E
enum filter_types_e { // enumeration as part of this object should be non-conflictive with other declarations similar.
	FLT_LOPASS=0,
	FLT_HIPASS,
	FLT_BANDPASS,
	FLT_NOTCH,
	FLT_PARAEQ,
	FLT_LOSHELF,
	FLT_HISHELF,
	FLT_NONE
};
#define FILTER_TYPES_E
#endif

void suckBiquads(int16_t* buf16, float* coefs);
int16_t peakBiquads(int16_t buf16, float* coefs); // for those times you wish you had time to take a peak reading before applying filters.


/*
FASTRUN void intbiquadfloat(float* buffer, float* coefs, int16_t* source);
FASTRUN void floatbiquadfloat(float* buffer, float* coefs);
FASTRUN void floatbiquadint(float* buffer, float* coefs, int16_t* dest);
FASTRUN void calcBiquad(uint8_t type, float Fc, float Q, float peakGain, float * coefs, float Fs)
*/

#endif

