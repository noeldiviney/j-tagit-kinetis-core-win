

class butts
{
  public:
//    typedef void (*callback_t)(const char*);
    typedef void (*callback_t)();
    butts(unsigned char pin, unsigned char int_ms=10, unsigned char butActive=LOW) { init(pin,int_ms,butActive); }
    
    void update()
    {
      if(digitalRead(pin_)==buttonActive)
      {
        if(flags&128) return;
        if((flags&1)==0)
        {
          if((flags&2)==0) rms=ms_=0; else ms_=0;
          flags|=1;
        } else if((flags&2)==0&&ms_>int_ms_) {
          flags|=2;
          if(press_!=nullptr) press_(); else flags|=8;
        } else if(longPressMillis>0&&rms>longPressMillis) {
          held=rms;
          if(tapped_!=nullptr) tapped_(); else flags|=4;
          flags|=192; // it is now a shift button and the release needs to be ignored.
        }
      } else {
        if(flags&1)
        {
          ms_=0;
          held=rms;
          flags&=~1;
        } else if((flags&2)!=0&&ms_>int_ms_) {
          if((flags&128)==0)
          {
            if(release_!=nullptr) release_(); else flags|=16;
            if(tapped_!=nullptr) tapped_(); else flags|=4;
          }
          flags&=~194;
        }
      }
    }
    
    bool tapped()
    {
      if((flags&4)==0) return false;
      flags&=~4;
      return true;
    }
    
    bool shift()
    {
      if(flags&64) return true;
      if(flags&2)
      {
        flags|=192; // shifted and ignore
        return true;
      }
      return false;
    }
    
    bool state()
    {
      if(flags&2) return true;
      return false;
    }
    
    bool press()
    {
      if((flags&8)==0) return false;
      flags&=~8;
      return true;
    }
    
    bool release()
    {
      if((flags&4)==0) return false;
      flags&=~4;
      return true;
    }

    unsigned long held;
    unsigned long longPressMillis=0;
    
    unsigned char buttonActive=LOW;
    
    void ignore() { if((flags&3)!=0) flags|=128; }
    
    void tappedCallback(callback_t callback_) { tapped_=callback_; }
    void pressCallback(callback_t callback_) { press_=callback_; }
    void releaseCallback(callback_t callback_) { release_=callback_; }
    
  private:
    void init(unsigned char pin, unsigned char int_ms, unsigned char butActive) {int_ms_=int_ms; pin_=pin; buttonActive=butActive; }
    unsigned char int_ms_;
    elapsedMillis ms_,rms;
    unsigned char pin_;
    unsigned char flags;
/*  flags;
b7: Clear and reset with no callbacks when button released, flags=\0\;
b6: Shift(); b6 is set by shift() if state() returns true then bit is used as response till release_() occurs.
b5: 
b5: released();       16
b3: pressed();        8
b2: tapped();         4
b1: state();          2
b0: last digital read 1

*/
    
    callback_t tapped_,press_,release_;

};