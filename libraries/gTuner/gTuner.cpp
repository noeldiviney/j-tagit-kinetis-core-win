

#include <inttypes.h>


#include "gTuner.h"


#define OCTAVA	31.785

// #define DETECTION_SCOPE 333
#define DEFAULT_TOLERANCE 0.033333

#define TOLERANCE_DIVIDER 17.303

#define DUMPTIME 249

#define LED_LOOSE 0
#define LED_TUNED 1
#define LED_TIGHT 2

#define F_TMR 500000

void gTuner::begin(void)
{
//	__disable_irq();
#if defined(__ROBS_ERROR_TRAP__)
	error_code_led=0x6f;  // bizarre, how could this possibly be the point of failure? EASY! Enable the FPU and it won't fail here.
#endif
	for(uint8_t n=0;n<12;n++) { nTol[n]=notes[n]*(DEFAULT_TOLERANCE/TOLERANCE_DIVIDER); }
#if defined(__ROBS_ERROR_TRAP__)
	error_code_led=0x6e;
#endif
//	__enable_irq();
}

FASTRUN void gTuner::inSample(void)
{
	int32_t temp=gTimer;
	static uint8_t dirt=1;
	dirt=1-dirt;
	if(temp>lastSample[dirt]) inSamples[dirt]=temp-lastSample[dirt];
	lastSample[dirt]=temp;
}

void gTuner::Tolerance(double val)
{
	for(uint8_t n=0;n<12;n++) { nTol[n]=notes[n]*(val/TOLERANCE_DIVIDER); }
}

uint16_t gTuner::update(void)
{
	static int32_t acc1=0;
	static uint8_t ctr0=0,ctr1=0,hits=0;
	static int32_t real_scope;
	static double avg1,avg2;
	
	myDir=1-myDir;
	if(inSamples[myDir])
	{
		// int32_t temp=inSamples[myDir];
		accum[myDir][0]=samples[myDir][sptr[myDir]--]=inSamples[myDir]; // n+0, n+7; this sample added to
		sptr[myDir]&=7;
		accum[myDir][0]+=samples[myDir][sptr[myDir]--]; // n+7, n+6; the last sample
		sptr[myDir]&=7;
		accum[myDir][1]=samples[myDir][sptr[myDir]--]; // n+6, n+5; then second last added to
		sptr[myDir]&=7;
		accum[myDir][1]+=samples[myDir][sptr[myDir]--]; // n+5, n+4; third last
		sptr[myDir]&=7;
		accum[myDir][2]=samples[myDir][sptr[myDir]--]; // n+4, n+3; then forth last added to
		sptr[myDir]&=7;
		accum[myDir][2]+=samples[myDir][sptr[myDir]--]; // n+3, n+2; fifth last
		sptr[myDir]&=7;
		accum[myDir][3]=samples[myDir][sptr[myDir]--]; // n+2, n+1; then sixth last sample added
		sptr[myDir]&=7;
		accum[myDir][3]+=samples[myDir][sptr[myDir]]; // n+1; to the seventh last sample.
		
/*		if(flags&1)
		{
			Serial.print(inSamples[myDir],DEC);
			if(myDir) Serial.println(); else Serial.print(" ");
		}
*/		cStep=1;

		inSamples[myDir]=0;
	} else if(cStep) {

//		if(flags&1) Serial.printf("%u ",cStep);
		double avg3=0;
		switch(cStep) {
		case 1:
			// real_scope=(accum[0][0]+accum[0][1]+accum[0][2]+accum[0][3]+accum[1][0]+accum[1][1]+accum[1][2]+accum[1][3])/(16*tuning_scope); // there is the accumulation of 16 samples in those accums
			if(runningAvg>500)
			{
				real_scope=runningAvg*(tuning_scope/3);
				if(real_scope<10) real_scope=10;
			} else {
				real_scope=100;
			}
//			Serial.printf("Using scope: %u ",real_scope);
		break;
		case 2:
			ctr0=0;
			if(abs(accum[0][0]-accum[0][1])<real_scope) acc0[ctr0++]=accum[0][0]+accum[0][1]; // 00-01
			if(abs(accum[0][0]-accum[0][2])<real_scope) acc0[ctr0++]=accum[0][0]+accum[0][2]; // 00-02
			if(abs(accum[0][0]-accum[0][3])<real_scope) acc0[ctr0++]=accum[0][0]+accum[0][3]; // 00-03
		break;
		case 3:
			if(abs(accum[0][1]-accum[0][2])<real_scope) acc0[ctr0++]=accum[0][1]+accum[0][2]; // 01-02
			if(abs(accum[0][1]-accum[0][3])<real_scope) acc0[ctr0++]=accum[0][1]+accum[0][3]; // 01-03
			if(abs(accum[0][2]-accum[0][3])<real_scope) acc0[ctr0++]=accum[0][2]+accum[0][3]; // 02-03
		break;
		case 4:
			if(abs(accum[1][0]-accum[1][1])<real_scope) acc0[ctr0++]=accum[1][0]+accum[1][1]; // 10-11
			if(abs(accum[1][0]-accum[1][2])<real_scope) acc0[ctr0++]=accum[1][0]+accum[1][2]; // 10-12
			if(abs(accum[1][0]-accum[1][3])<real_scope) acc0[ctr0++]=accum[1][0]+accum[1][3]; // 10-13
		break;
		case 5:
			if(abs(accum[1][1]-accum[1][2])<real_scope) acc0[ctr0++]=accum[1][1]+accum[1][2]; // 11-12
			if(abs(accum[1][1]-accum[1][3])<real_scope) acc0[ctr0++]=accum[1][1]+accum[1][3]; // 11-13
			if(abs(accum[1][2]-accum[1][3])<real_scope) acc0[ctr0++]=accum[1][2]+accum[1][3]; // 12-13
//			Serial.printf("SamplePairs: %u ",ctr0);
		break;
		case 6:
			if(ctr0<minSamplePairs) cStep=254; // get out of here.
		break;
		case 7:
			ctr1=0;
			acc1=1;
		break;
		case 8:
			hits=1;
			avg1=acc0[ctr1];
			for(uint8_t num=0;num<ctr0;num++) {
				if(num!=ctr1)
				{
					if(abs(acc0[ctr1]-acc0[num])<real_scope)
					{
						hits++;
						avg1+=acc0[num];
						if(hits>acc1)
						{
							avg2=avg1;
							acc1=hits;
						}
					}
				}
			}
			ctr1++;
			if(ctr1<ctr0&&hits+1<ctr0) cStep--; // if that was not the last valid sample to check against and it didn't match all other valid samples then check the next one
		break;
		case 9:
			if(acc1<minMatchedSamples) cStep=253; // number of sample accumulators that match, previously acc1<3 but this version was missing detections prior version was catching.
//			Serial.printf("MatchedSamples: %u ",acc1);
		break;
		case 10: // SNAP!! That is enough to make a conclusion from.
			avg3=avg2/((double)acc1*8);
			
//			Serial.printf("avg3: %f ",avg3);
			
			if(fabs(avg3-runningAvg)<real_scope)
			{
				if(flags&2) myAnswer|=128; // set bit 7 to indicate detections are taking place.
				detectedAvg=(runningAvg+avg3+addAvgs[0]+addAvgs[1]+addAvgs[2]+addAvgs[3])/6;
				addAvgs[aptr++]=runningAvg;
				aptr&=3;
				runningAvg=avg3;
				dumpit=0;
//				Serial.print("(Matched runningAvg) ");
			} else {
				addAvgs[0]=addAvgs[1]=addAvgs[2]=addAvgs[3]=runningAvg=avg3;
//				Serial.print("(Replaced runningAvg) ");
			}
			cStep=255;
		break;
		case 254:
			if(flags&2) myAnswer&=~128; // unset bit 7 to indicate detections are not taking place.
		}
		cStep++;
//		if((flags&1)&&cStep==0) Serial.println();
	} else if(dumpit>DUMPTIME) {
		detectedAvg=F_TMR;
		dumpit=0;
		if(flags&2) myAnswer&=~128; // unset bit 7 to indicate detections are not taking place.
//		if(flags&1) Serial.println("Dumped it");
	} else if(fabs(detectedAvg-lastAvg)>0.00999) {
		lastAvg=detectedAvg;
//		detectedFreq=0;
//		myAnswer=(myAnswer&63488)|(0x7<<8)|12;
		lastDiff=0;
//		Serial.print("det: ");
//		Serial.println(lastAvg,DEC);
		if(lastAvg<F_TMR-1&&lastAvg>0)
		{
			double myFreq=F_TMR/lastAvg;
			if(myFreq>minFreq) 
			{
				detectedFreq=myFreq;
				while(myFreq>OCTAVA) myFreq/=2;

				unsigned char this_note=12;
				double diff=30,tdif=30;
				for(unsigned char cnta=0;cnta<12;cnta++) {
					tdif=abs(myFreq-notes[cnta]);
					if(tdif<diff) {
						this_note=cnta;
						diff=tdif;
						lastDiff=-(myFreq-notes[cnta])*ratios[cnta]; // 
					}
				}
				uint8_t leds=0x7; // bit 0 = loose, bit 1 = tuned, bit 2 = tight
				
				if(diff<nTol[this_note]) {
					leds&=(~(1<<LED_TUNED)); // tuned nicely.
				} else {
					// if(diff<nTol[this_note]*1.5) leds&=(~(1<<LED_TUNED)); // turn on the green LED for being so close anyway.
					if(myFreq<notes[this_note]) {
						leds&=(~(1<<LED_LOOSE)); // loose
					} else {
						leds&=(~(1<<LED_TIGHT)); // tight
					}
				}
				myAnswer=(myAnswer&63488)|(leds<<8)|this_note;
			} else {
				if(flags&2) myAnswer&=~128; // unset bit 7 to indicate detections are not taking place.
				detectedFreq=0;
				myAnswer=(myAnswer&63488)|(0x7<<8)|12;
			}
		} else {
			if(flags&2) myAnswer&=~128; // unset bit 7 to indicate detections are not taking place.
			detectedFreq=0;
			myAnswer=(myAnswer&63488)|(0x7<<8)|12;
		}
	}
	return myAnswer;
}

